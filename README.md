# `git-workshop2018` materials

This repo contains materials for a git workshop that [I][adamliter] did
for folks at the University of Marlyand in the summer of 2018.

[adamliter]: https://adamliter.org
