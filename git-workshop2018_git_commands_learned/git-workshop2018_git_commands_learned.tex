% !TEX encoding = UTF-8 Unicode
% !TEX TS-program = arara
% arara: xelatex: { shell: yes }
% arara: xelatex: { shell: yes, synctex: yes }

\documentclass{article}

% -----------------------------------------------------------------------------
% FONTS
% -----------------------------------------------------------------------------
\usepackage{fontspec}

\setmainfont
  [
    UprightFont = *-Regular,
    BoldFont = *-Semibold,
    ItalicFont = *-Italic,
    BoldItalicFont = *-SemiboldItalic
  ]
  {AGaramondPro}

\setmonofont{Inconsolata}

% -----------------------------------------------------------------------------
% GENERAL DOCUMENT SETUP STUFF
% -----------------------------------------------------------------------------
\usepackage{geometry}

\geometry{
  lmargin=2in
}

\usepackage{parskip}

\usepackage{enumitem}

\newlist{gitcommands}{description}{1}
\setlist
  [gitcommands]
  {
    align=right,
    leftmargin=*,
    font=\ttfamily
  }

% -----------------------------------------------------------------------------
% SYNTAX HIGHLIGHTING
% -----------------------------------------------------------------------------
\usepackage{minted}
\setminted{frame=single}

% -----------------------------------------------------------------------------
% HYPERLINKS & COLORS
% -----------------------------------------------------------------------------
\usepackage{xcolor}
\definecolor{darkred}{HTML}{B22613}

\usepackage
  [
    colorlinks
  ]
  {hyperref}

\hypersetup{
  linkcolor=darkred,
  citecolor=gray,
  urlcolor=cyan
}

% -----------------------------------------------------------------------------
% METADATA
% -----------------------------------------------------------------------------

\title{\GIT{} commands cheat sheet}
\author{Adam Liter\\\Email{git@adamliter.org}}
\date{Last updated: \today}

% -----------------------------------------------------------------------------
% MACROS
% -----------------------------------------------------------------------------
\newcommand*{\Email}[1]{\href{mailto:#1}{\nolinkurl{#1}}}

\newcommand*{\GIT}{\texttt{git}}

\newcommand*{\GC}[1]{\texttt{#1}}
\newcommand*{\FileName}[1]{\texttt{#1}}

\newcommand*{\IE}{\emph{i.e.},}
\newcommand*{\EG}{\emph{e.g.},}

\begin{document}

\maketitle

\begin{abstract}
This is a cheat sheet for \GIT{} commands that we've learned at each of the \GIT{} workshop meetings.
Remember that all \GIT{} commands start with \GIT{}, followed by a space, and then the name of the \GIT{} subcommand.
You can also append a space and then \mintinline{sh}{--help} to the subcommand in order to get some information about how to use the subcommand.
\end{abstract}

\section*{Meeting 1}

\begin{gitcommands}
  \item[git --version]{This prints \GIT{} version information.}
\end{gitcommands}

\section*{Meeting 2}

\begin{gitcommands}
  \item[git config]{This command allows you to configure \GIT{} settings at three different levels: the system level (\GC{--system}), the global level (\GC{--global}), and the local (repository) level (\GC{--local}). This is how you can set your name and email address for every repository that your user works on, using the \GC{--global} flag.}
  \item[git init]{This command initializes a repository in a directory (\IE{} a folder).}
  \item[git status]{This command displays information about the status of your working tree and the index (\IE{} the staging area).}
  \item[git add]{This command allows you to add new or changed files to the index (\IE{} the staging area).}
  \item[git commit]{This command allows you to commit whatever changes are staged in your index; you must specify a commit message, which you can do with the \GC{-m} flag and a quoted message or by omitting the \GC{-m} flag and typing the commit message into the editor that opens up.}
  \item[git log]{This command allows you to inspect the commit history of your repository.}
\end{gitcommands}

\section*{Meeting 3}

\begin{gitcommands}
  \item[git revert <HASH>]{This commands allows you to introduce a new commit that undoes the changes of a previous commit. Pass the hash of the commit that you would like to undo as an argument to the command.}
  \item[git reset <HASH>]{In this usage, \GC{git reset} moves the current branch ref (or pointer) and the HEAD ref (or pointer), making both of them point to the commit identified by \texttt{<HASH>}. Furthermore, there are three options: \GC{--hard}, \GC{--mixed} (the default, if unspecified), and \GC{--soft}. The \GC{--hard} option rolls back the local version history, the index, and the working tree to the commit identified by \texttt{<HASH>}; \GC{--mixed} rolls back the local version history and the index to the commit identified by \texttt{<HASH>}; and \GC{--soft} only rolls back the local version history to the commit identified by \texttt{<HASH>}.}
  \item[git reset (HEAD) <PATH>]{In this usage, where \texttt{<PATH>} is the file path (or name) of one or more files, \GC{git reset} removes \texttt{<PATH>} from the index (\IE{} it unstages those files, behaving like the opposite of \GC{git add}). Specifying \GC{HEAD} as the second argument of this command is optional here, as indicated by the parentheses (the parentheses are \emph{not} part of the command).}
  \item[git diff]{This command allows you to see a diff of any new changes. Without any options, it shows the differences between your working tree and whatever HEAD points to (which, in the usual case, is equivalent to the most recent commit in your local version history). With the \GC{--cached} option, it shows the differences between your index and HEAD.}
\end{gitcommands}

\section*{Meeting 4}

\begin{gitcommands}
  \item[ssh-keygen]{This is \emph{not} a \GIT{} command, but it's worth mentioning because it's the command that allows you to create an SSH keypair, which can be used to authenticate to remote \GIT{} repositories, such as remote repositories that are hosted on GitHub, GitLab, or Bitbucket. It takes a \GC{-t} flag, which specifies the algorithm for generating the keypair; you should use \GC{rsa}. It takes a \GC{-b} flag to specify the number of bits to use; you should use \GC{4096}. Finally, it takes a \GC{-C} flag to specify a comment. Use the command like \GC{ssh-keygen -t rsa -b 4096 -C "$<$YOUR\_COMPUTER\_NAME\_HERE$>$"}. It's possible that the suggested algorithm and number of bits that you should use will change in the future. Check \href{https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/#generating-a-new-ssh-key}{GitHub's guide to ``Generating a new SSH Key''} for current best practices; follow whatever recommendations they suggest.}
  \item[git remote add]{The command \GC{git remote} is a \GIT{} subcommand that itself has several further subsubcomands. \GC{git remote add} is one of these, and it allows you to set up a remote repository for your local repository, to which you can send changes and from which you can receive changes. The command takes two arguments. The first is the name that you want to give the remote repo, and the second is the URL for the remote repository. For your main remote repository (you can have multiple), you should use the name \GC{origin}; this name is a \GIT{} convention, and you should stick to it unless you have a good reason not to. The second argument will be either an SSH URL or an HTTPS URL. You can usually see the URL if you view the repository in a web browser on GitHub, GitLab, or Bitbucket; they will have it set up so that you can easily copy and paste it.}
  \item[git push]{This command allows you to push local changes on a local branch to a branch on a remote repository. It takes two arguments. The first is the remote repository, and the second is the branch name. For example, \GC{git push origin master} will push your local changes on your local \GC{master} branch to the remote \GC{master} branch on the remote \GC{origin} repository. Furthermore, if you use the \GC{-u} option (\IE{} \GC{git push -u origin master}), you can set these as the default arguments, and then you can just do \GC{git push} without any arguments in the future.}
  \item[git rm]{This command can be used to both delete a file from your working tree \emph{and} stage the fact that you've deleted that file in your index.}
  \item[git mv]{This command can be used to ``move'' (\IE{} rename) a file in your working tree \emph{and} stage the fact that you've renamed the file in your index. It takes two arguments. The first is the current name of the file, and the second is the new name that you want to give to that file.}
\end{gitcommands}

\section*{Meeting 5}

\begin{gitcommands}
  \item[git lfs install]{This is a subcommand of the extension to \GIT{} called \href{https://git-lfs.github.com/}{Git LFS}, which is a tool for tracking binary files with \GIT. It does two things. First, it adds some configuration options to your global \GIT{} configuration file (by default, the file called \FileName{\~{}/.gitconfig}). Second, if run from inside of a \GIT{} repository, this command will set up that \GIT{} repository to use Git LFS; in this sense, it is sort of like \GC{git init}. You will need to run it once inside each repository that you would like to use Git LFS with.}
  \item[git lfs track]{This command takes a file name, file path, or glob pattern that specifies files that you would like Git LFS to track. You'll need to run this before you stage changes to binary files. For example, if you want to use Git LFS to track all \FileName{.png} files, you could run \GC{git lfs track "*.png"}. After that, you can stage and commit \FileName{.png} files. Note that the quotes in \GC{git lfs track} command are important. Running this command adds some content to your \FileName{.gitattributes} file, which you should stage and commit, so that all of your collaborators have the same Git LFS settings.}
  \item[git fetch]{This is one of the few \GIT{} commands that sends or receives information over the network (\IE{} internet). It takes one argument, which is either a remote name or a URL that specifies a remote \GIT{} repository. If you omit this argument, it will first try to fetch changes from the default remote, and, if no default remote exists, then it will try to fetch changes from a remote called \GC{origin} (this is one reason why you should follow the remote naming convention). If you have multiple remotes and want to fetch changes from all remotes, use the \GC{--all} flag.}
  \item[git rebase]{This command takes a series of commits and reapplies those commits on top of another commit. That is to say, it can be used to change the parent commits of commits that you've already made. It takes two arguments. The first argument specifies the branch on top of which you'd like to reapply a series of commits, and the second argument specifies the commits that you'd like to reapply. In the usual case, you might run \GC{git rebase origin/master master}, which would reapply all of the commits that have been made on the \GC{master} branch since its point of divergence from the branch called \GC{origin/master} on top of the branch called \GC{origin/master}. You can omit the second argument, and \GC{git rebase} will use the branch that you're currently on as the second argument. So you can just run \GC{git rebase origin/master} if you're already on the \GC{master} branch in order to reapply divergent commits on the \GC{master} branch on top of the \GC{origin/master} branch.}
\end{gitcommands}

\section*{Meeting 6}

\begin{gitcommands}
  \item[eval \$(ssh-agent)]{This is \emph{not} a \GIT{} command, but it's worth mentioning because it can be used to help cache your SSH credentials in memory, so that you don't have to enter the password for your private SSH key all of the time. This command evaluates the output of the \GC{ssh-agent} command; evaluating the output of that command sets up some environment variables and starts a process on your computer that the \GC{ssh-add} command can talk to. Note that you only need to run this command on Linux or Windows, not on Mac, because all terminal sessions are started as login shells by default on macOS. Don't worry too much about what this means, just know that you don't need to do this on a Mac (and if you do want to worry about what this means, see \href{https://unix.stackexchange.com/q/38175/60639}{here}).}
  \item[ssh-add]{This \emph{not} a \GIT{} command, but it's worth mentioning because it can be used to cache your SSH credentials in memory, so that you don't have to enter the password for your private SSH key all of the time. This command optionally takes the \GC{-t} flag, which allows you to specify how long you want your SSH credentials cached for. By default, this is in seconds, but you can use \GC{m} for minutes, \GC{h} for hours, \GC{d} for days, and \GC{w} for weeks. The main argument that this command takes is a file path to your SSH private key's location on your hard drive. If you wanted to cache your SSH private key for 6 hours, you'd run \GC{ssh-add -t 6h \~{}/.ssh/id\_rsa}, assuming your private key was called \FileName{id\_rsa} and was located in the folder \FileName{\~{}/.ssh}.\footnote{On Windows and Linux where you have to explicitly start a new SSH agent process each time you open the command line, you won't be able to use your cached credentials from before, even if the time limit you specified hasn't yet fully elapsed. For example, if you cache your credentials for 6 hours, work in the command line for an hour, close the command line, and then open it again twenty minutes later, it will appear as if your SSH credentials are no longer cached. You can work around this by finding the SSH agent process that you started in your first shell session (see, \EG{} \href{http://blog.joncairns.com/2013/12/understanding-ssh-agent-and-ssh-add/}{here} and \href{https://github.com/wwalker/ssh-find-agent}{here}), or you can just start a new SSH agent process by running \GC{eval \$(ssh-agent)} again.}}
  \item[git clone]{This command takes two arguments, the second of which is optional. The first argument is a URL---usually an HTTPS or SSH URL---that points to the remote repository that you want to create a local copy of. The second argument is a file path that specifies where you want to create this local copy. If the second argument is omitted, it creates a copy of that remote repository in the directory that you're already in, and it uses the same folder name that the remote repository uses.}
  \item[git branch]{This command can be used to list, create, and delete branches. \GC{git branch -v} will list all branches in your local repository. \GC{git branch -a} will list all branches in your local repository, including local remote-tracking branches (\EG{} \GC{origin/master}). \GC{git branch $<$BRANCH$>$} will create a new branch called \GC{$<$BRANCH$>$} that points to whatever HEAD points to. \GC{git branch -d $<$BRANCH$>$} deletes \GC{$<$BRANCH$>$}, as long as it has been merged into the \GC{master} branch. \GC{git branch -D $<$BRANCH$>$} deletes \GC{$<$BRANCH$>$}, regardless of whether it has been merged into the \GC{master} branch (note that this will possibly create dangling commits in your repository).}
  \item[git checkout]{This command moves the HEAD pointer to point to whatever the argument to the command is. The argument to the command is usually a branch name, since HEAD is a pointer that usually points to other pointers (\IE{} branches). However, it is possible to \GC{git checkout} a commit, which leads to a detached HEAD state (because HEAD now points directly to a commit, rather than pointing to a branch name). \emph{Importantly}, this command changes the files in your working directory to match whatever the state of affairs is according to the branch (or commit) that you've just checked out.}
  \item[git merge]{This command takes one argument, usually a branch name (but it can also be a commit hash). It merges the changes from that branch (or commit) into the branch that you currently have checked out. If possible, it will do a ``fast-forward'' merge, which means that it will just advance the pointer of the branch that you currently have checked out to point to whatever the branch argument that you passed in to the command points to. If this is not possible because the commits on that branch are not direct descendants of your currently checked out branch, then it will create a merge commit, which is a commit, usually empty, that has multiple parent commits. If you're merging a feature branch in to the \GC{master} branch, you usually want to avoid a fast-forward merge, even if it is possible, so you should specify the \GC{--no-ff} option (\IE{} \GC{git merge --no-ff $<$BRANCH$>$}).}
  \item[git show $<$HASH$>$]{This command shows the contents of the commit specified by \GC{$<$HASH$>$}, including the commit metadata, the commit author, the commit message, and the changes introduced by that commit.}
\end{gitcommands}

\end{document}